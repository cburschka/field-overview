<?php
/**
 * Created by PhpStorm.
 * User: christoph
 * Date: 06.10.16
 * Time: 09:49
 */

namespace Drupal\field_overview;


use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

class FieldOverview {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityField;
  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  private $bundleInfo;
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityType;

  public function getFieldInstances() {
    $data = $this->bundleInfo->getAllBundleInfo();

    $output = [];

    foreach ($data as $entity_type_id => $bundles) {
      // Skip the entity types that cannot have fields.
      $entity_type = $this->entityType->getDefinition($entity_type_id);
      if (!$entity_type->isSubclassOf(FieldableEntityInterface::class)) {
        continue;
      }

      foreach ($bundles as $name => $bundle) {
        $output[$name] = [
          'label' => $bundle['label'],
          'fields' => [],
        ];

        $fields = $this->entityField->getFieldDefinitions($entity_type_id, $name);

        $output[$name]['fields'] = $fields;
      }
    }

    return $output;
  }

  /**
   * FieldOverview constructor.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityField
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityType
   * @internal param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   */
  public function __construct(EntityFieldManagerInterface $entityField,
                              EntityTypeBundleInfoInterface $bundleInfo,
                              EntityTypeManagerInterface $entityType
  ) {
    $this->entityField = $entityField;
    $this->bundleInfo = $bundleInfo;
    $this->entityType = $entityType;
  }
}
