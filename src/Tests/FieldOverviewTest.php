<?php
/**
 * Created by PhpStorm.
 * User: christoph
 * Date: 06.10.16
 * Time: 13:35
 */

namespace Drupal\field_overview\Tests;


use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\simpletest\WebTestBase;

/**
 * Test the field overview page.
 *
 * @group field_overview
 */
class FieldOverviewTest extends WebTestBase  {
  public static $modules = [
    'field_overview',
    'node',
  ];

  protected $adminUser;
  protected $webUser;

  /**
   * @var EntityInterface
   */
  private $type;

  /**
   * @var EntityInterface
   */
  private $field;

  protected function setUp() {
    parent::setUp();

    $this->webUser = $this->drupalCreateUser();
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer content types',
    ]);

    $this->type = $this->drupalCreateContentType([
      'type' => 'field_overview_test_type',
      'name' => 'Content type: ' . $this->randomString(),
    ]);

    $field_storage = [
      'field_name' => 'field_name_' . strtolower($this->randomMachineName()),
      'entity_type' => 'node',
      'type' => 'text',
    ];
    FieldStorageConfig::create($field_storage)->save();
    $this->field = [
      'field_name' => $field_storage['field_name'],
      'label' => 'Field label: ' . $this->randomString(),
      'entity_type' => 'node',
      'bundle' => $this->type->id(),
    ];
    FieldConfig::create($this->field)->save();
  }

  protected function testPage() {
    $this->drupalLogin($this->webUser);
    $this->drupalGet('/admin/structure/fields');
    $this->assertResponse(403, "Ensure the page is restricted to administrators.");

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('/admin/structure');

    $this->clickLink('Field instances');

    $this->assertTitle('Field instances | Drupal');

    $this->assertText(Html::escape($this->type->label()));
    $this->assertText($this->field['field_name']);
    $this->assertText(Html::escape($this->field['label']));
  }
}