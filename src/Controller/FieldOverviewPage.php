<?php
/**
 * Created by PhpStorm.
 * User: christoph
 * Date: 06.10.16
 * Time: 09:51
 */

namespace Drupal\field_overview\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\field_overview\FieldOverview;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FieldOverviewPage extends ControllerBase  {

  /**
   * @var \Drupal\field_overview\FieldOverview
   */
  private $fieldOverview;

  public function main() {
    $data = $this->fieldOverview->getFieldInstances();

    $output = [
      '#title' => $this->t('Field instances'),
    ];

    foreach ($data as $name => $bundle) {
      $fields = $bundle['fields'];

      $output[$name] = [
        '#type' => 'details',
        '#title' => $bundle['label'],
        '#open' => TRUE,
      ];

      foreach ($fields as $id => $field) {
        $output[$name][$id] = [
          '#theme' => 'field_overview_field',
          '#field' => $field,
        ];
      }
    }

    return $output;
  }

  /**
   * FieldInstancePage constructor.
   * @param \Drupal\field_overview\FieldOverview $fieldOverview
   */
  public function __construct(FieldOverview $fieldOverview) {
    $this->fieldOverview = $fieldOverview;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('field_overview'));
  }
}