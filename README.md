# field-overview

This is a small proof of concept module demonstrating the use of the
Drupal 8 infrastructure, including:

- routes and controllers
- services and dependency injection
- entity types, bundles and fields
- Twig templates
- Composer packaging

The basic feature of the module is to add an administrative page at
`/admin/structure/fields` which lists every field instance attached to every
content type in the Drupal 8 installation.